export const state = () => ({
  userData : {}
})

export const mutations = {
  INIT_USER(state, user) {
    state.userData = user
  }
}

export const actions = {
  initUser({commit}) {
    commit('INIT_USER', 
      {
        name : 'Fabien Kircher',
        contactDetails: [
          {
            id:1,
            adressName:'maison',
            adress:'67, rue de pont à mousson',
            postCode:'57950',
            city:'Montigny lès Metz',
          },
        ],
      }
    )
  }
}